import json
import os.path
import sys
import time
from configparser import ConfigParser
from pathlib import Path

import click
import gitlab
import requests
from rich.console import Console
from rich.prompt import Confirm

DEFAULT_CFG = "config.ini"


def test_gitlab(console, gl: gitlab.Gitlab):
    try:
        gl.auth()
    except gitlab.exceptions.GitlabAuthenticationError:
        console.log(":x:  Authentication Error!", style="bold red")
        sys.exit(1)
    except requests.exceptions.ConnectionError:
        console.log(":x:  Error connecting to Gitlab!", style="bold red")
        sys.exit(1)
    console.log(
        ":heavy_check_mark: Connection to gitlab established successfully!",
        style="green",
    )


def split_name_and_namespace(path):
    parts = path.split("/")
    namespace = "/".join(parts[:-1])
    name = "".join(parts[-1:])
    return name, namespace


def configure(ctx, param, filename):
    cfg = ConfigParser()
    cfg.read(filename)
    try:
        options = dict(cfg["global"])
    except KeyError:
        options = {}
    ctx.default_map = options


@click.command()
@click.option(
    "-c",
    "--config",
    type=click.Path(dir_okay=False),
    default=DEFAULT_CFG,
    callback=configure,
    is_eager=True,
    expose_value=False,
    help="Config file",
    show_default=True,
)
@click.option(
    "--debug",
    type=click.BOOL,
    default=False,
    is_eager=False,
    help="Enable Debug Mode",
    show_default=True,
)
@click.option(
    "--source-url",
    type=click.STRING,
    default="",
    is_eager=False,
    help="Url of the source gitlab instance",
    show_default=True,
)
@click.option(
    "--source-token",
    type=click.STRING,
    default="",
    is_eager=False,
    help="Personal Access Token for the source gitlab instance",
    show_default=False,
)
@click.option(
    "--destination-url",
    type=click.STRING,
    default="",
    is_eager=False,
    help="Url of the destination gitlab instance",
    show_default=True,
)
@click.option(
    "--destination-token",
    type=click.STRING,
    default="",
    is_eager=False,
    help="Personal Access Token for the destination gitlab instance",
    show_default=False,
)
@click.option(
    "--temp-dir",
    type=click.Path(file_okay=False, dir_okay=True, writable=True),
    default=".",
    help="Directory for temporary data",
    show_default=True,
)
@click.argument("source-path", type=click.STRING)
@click.argument("destination-path", type=click.STRING)
def main(source_path, destination_path, **kwargs):
    console = Console()
    temp_dir = Path(kwargs["temp_dir"])
    export_file = Path.joinpath(temp_dir, "export.tgz")

    if os.path.isfile(export_file):
        console.log(":exclamation_mark: Found old export file!")
        delete = Confirm.ask("Delete old file?")
        if delete:
            os.remove(export_file)
        else:
            console.log(":x:  Error file exists!", style="bold red")
            sys.exit(1)
    # Export project from source
    console.log("Exporting project from source", style="bold yellow underline")
    console.log(":electric_plug: Connecting to source gitlab")
    source_gitlab = gitlab.Gitlab(
        kwargs["source_url"], private_token=kwargs["source_token"]
    )
    test_gitlab(console, source_gitlab)
    console.log(":arrow_right: Trying to find project ", source_path)
    try:
        source_project = source_gitlab.projects.get(source_path)
    except gitlab.exceptions.GitlabGetError:
        console.log(":x:  Project not found!", style="bold red")
        sys.exit(1)
    console.log(":heavy_check_mark: Project found!", style="green")
    console.log(":arrow_right: Trigger export file generation")
    export = source_project.exports.create()
    console.log(":heavy_check_mark: Export started!", style="green")
    status = console.status("[bold yellow]Waiting for export...")
    status.start()
    export.refresh()
    while export.export_status != "finished":
        time.sleep(1)
        export.refresh()
    status.stop()
    console.log(":heavy_check_mark: Export finished!", style="green")
    console.log(":arrow_right: Stating export file download")
    status = console.status("[bold yellow]Downloading export file...")
    status.start()
    with open(export_file, "wb") as f:
        export.download(streamed=True, action=f.write)
    status.stop()
    console.log(":heavy_check_mark: Download finished!", style="green")
    archive = Confirm.ask("Archive old project? (Make read-only)")
    if archive:
        source_project.archive()
        console.log(":heavy_check_mark: Project archived!", style="green")
    # Import project into destination
    name, namespace = split_name_and_namespace(destination_path)
    console.log("Importing project into destination", style="bold yellow underline")
    console.log(":electric_plug: Connecting to destination gitlab")
    destination_gitlab = gitlab.Gitlab(
        kwargs["destination_url"], private_token=kwargs["destination_token"]
    )
    destination_gitlab.enable_debug()
    test_gitlab(console, destination_gitlab)
    console.log(":arrow_right: Trying to find project ", destination_path)
    try:
        destination_gitlab.projects.get(destination_path)
    except gitlab.exceptions.GitlabGetError:
        console.log(":heavy_check_mark: Project does not already exist!", style="green")
    else:
        console.log(":x:  Project already exists, Aborting!", style="bold red")
        sys.exit(1)
    console.log(":arrow_right: Trying to find namespace ", destination_path)
    try:
        gl_namespace = destination_gitlab.namespaces.get(namespace)
    except gitlab.exceptions.GitlabGetError:
        console.log(
            ":x:  Namespace not found, please create, Aborting!", style="bold red"
        )
        sys.exit(1)
    console.log(":arrow_right: Stating project import")
    status = console.status("[bold yellow]Uploading project export...")
    status.start()
    with open(export_file, "rb") as f:
        output = destination_gitlab.projects.import_project(
            f, path=name, name=name, namespace=gl_namespace.id
        )
    status.stop()
    console.log(":heavy_check_mark: Project imported!", style="green")


if __name__ == "__main__":
    main()
